 


$(function() {
    $( "#slider" ).slider({
      range: "max",
      value:100,
      min: 50,
      max: 500,
      step: 50,
      slide: function( event, ui ) {
        $( "#amount" ).val( "" + ui.value );
      }
    });
    $( "#amount" ).val( "" + $( "#slider" ).slider( "value" ) );
  });

  $(function() {
    $( "#slider2" ).slider({
      range: "max",
      value:14,
      min: 1,
      max: 60,
      step: 1,
      slide: function( event, ui ) {
        $( "#amount2" ).val( "" + ui.value );
      }
    });
    $( "#amount2" ).val( "" + $( "#slider2" ).slider( "value" ) );
  });