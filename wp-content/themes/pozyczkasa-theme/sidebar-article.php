    <aside class="col-md-3 col-sm-4 col-xs-12 sidebar-blog">
        <h3>Kategorie</h3>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
        		<div class="category-list">
        			<ul>
        				<li>Chwilówki</li>
        				<li>Kredyty</li>
        				<li>Dla Firm</li>
        				<li>Kredyt Hipoteczny</li>
        				<li>Bankowość</li>
        				<li>Nowe Technologie</li>
        			</ul>
        		</div>
        	</div>
        </div>
        <h3>Tagi</h3>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
        		<div class="category-list">
					<ul class="sidebar-category-tags">
						<li><a href="#" >Gotówka</a></li>
						<li><a href="#" >Oprocentowanie</a></li>
						<li><a href="#" >Banki</a></li>
						<li><a href="#" >Kredyt we Frankach</a></li>
						<li><a href="#" >Hipoteka</a></li>
						<li><a href="#" >Dom</a></li>
						<li><a href="#" >Zakup mieszkania</a></li>
						<li><a href="#" >Firma</a></li>
						<li><a href="#" >Samochód Firmowy</a></li> 
					</ul>
        		</div>
        	</div>
        </div>

        <h3>Reklama</h3>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
        		<div class="adw-slider">
					 <img src="img/assets/adw_example.png">
        		</div>
        	</div>
        </div>

        <!-- SIDEBAR SEARCH BOX --> 
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="box-search">
                        <h3>Wyszukaj oferty</h3>
                        <div class="input-group">
                        <input type="text" class="form-control" aria-label="...">
                        <div class="input-group-btn">
                            <!-- Buttons -->
                            <button type="button" class="btn btn-large btn-block btn-default blue-btn"><span class="glyphicon glyphicon-search"></span></button>
                        </div>
                    </div>
                 </div> 
            </div>
        <!-- SIDEBAR SEARCH BOX -->  
    </aside>
    <!-- SIDEBAR ITEM --> 