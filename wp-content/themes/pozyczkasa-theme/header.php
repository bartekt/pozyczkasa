 
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

  <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="<?php bloginfo('description'); ?>"> 
  <?php wp_head(); ?>

  <script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.js"></script> <!-- Modernizr -->

  <script>
    // conditionizr.com
    conditionizr.config({
        assets: '<?php echo get_template_directory_uri(); ?>',
        tests: {}
    });
    </script> 
</head> 
<body>

<div class="container">
    <div class="top">
        <a href="#" class="brand">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="logo">
        </a>
        <nav class="pull-right">
            <ul>
                <li><a href="/pozyczkasa">Strona Główna</a></li>
                <li><a href="page-o-nas.php">O nas</a></li>
                <li><a href="blog.php">Blog</a></li>
                <li><a href="template-kontakt.php">Kontakt</a></li>
            </ul>
        </nav>
    </div> 
</div>

<div class="container"> 
<!-- NAVBAR --> 
<nav class="navbar navbar-default" role="navigation"> 
<!-- Brand and toggle get grouped for better mobile display --> 
  <div class="navbar-header"> 
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> 
      <span class="sr-only">Toggle navigation</span> 
      <span class="icon-bar"></span> 
      <span class="icon-bar"></span> 
      <span class="icon-bar"></span> 
    </button>  
  </div> 
  <!-- Collect the nav links, forms, and other content for toggling --> 
  <div class="collapse navbar-collapse navbar-ex1-collapse"> 
    <ul class="nav navbar-nav"> 
      <li class="active"><a href="#">Chwilówki</a></li> 
      <li><a href="#">Kredyt gotówkowy </a></li>
      <li><a href="#"> Kredyt hipoteczny</a></li> 
      <li><a href="#">Konta osobiste</a></li>  
      <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Kredyty dla firm</a> 
        <ul class="dropdown-menu"> 
          <li><a href="#"><i></i>Darmowe Pożyczki</a></li> 
          <li><a href="#"><i></i>Pożyczki ratalne</a></li> 
          <li><a href="#"><i></i>kredyt na budowę domu</a></li> 
          <li><a href="#"><i></i>kredyt refinansowany</a></li> 
          <li><a href="#"><i></i>Kredyt konsolidacyjny</a></li>
          <li><a href="#"><i></i>Pozyczka hipoteczna</a></li>
          <li><a href="#"><i></i>kredyt walutowy</a></li>  
           
        </ul> 
      </li> 
    </ul>
  </div>
</nav>
<!-- NAVBAR -->
</div>
