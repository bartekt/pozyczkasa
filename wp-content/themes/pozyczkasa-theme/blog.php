<?php include("header.php"); ?>
<section id="pages"> 
    <div class="container"> 
        <div class="row">
            <div class="col-md-9 col-sm-8 col-xs-12">
            <h2 class="headline">Blog</h2> 

            <div class="row blog">  

            <!-- blog-post -->
            	<div class="col-md-4 col-sm-6 col-xs-12">
	            	<div class="item">
		            	<a href="single-blog.php">
		            		<img src="img/assets/b1.png" alt="b1">
		            		<div class="apla">
		            			<h3>Aenean nonummy hendrerit </h3>
		            			<span><time>02.02.2016</time> / Chwilówki</span>
		            			<p>
		            				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
		            			</p>
		            			<a class="permalink" href="#">Przejdz do wpisu »</a>
		            		</div>
		            	</a>
	            	</div>
            	</div>
            	<!-- blog-post-end -->

            	<!-- blog-post -->
            	<div class="col-md-4 col-sm-6 col-xs-12">
	            	<div class="item">
		            	<a href="">
		            		<img src="img/assets/b2.png" alt="b1">
		            		<div class="apla">
		            			<h3>Aenean nonummy hendrerit </h3>
		            			<span><time>02.02.2016</time> / Chwilówki</span>
		            			<p>
		            				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
		            			</p>
		            			<a class="permalink" href="#">Przejdz do wpisu »</a>
		            		</div>
		            	</a>
	            	</div>
            	</div>
            	<!-- blog-post-end -->

            	<!-- blog-post -->
            	<div class="col-md-4 col-sm-6 col-xs-12">
	            	<div class="item">
		            	<a href="">
		            		<img src="img/assets/b3.png" alt="b1">
		            		<div class="apla">
		            			<h3>Aenean nonummy hendrerit </h3>
		            			<span><time>02.02.2016</time> / Chwilówki</span>
		            			<p>
		            				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
		            			</p>
		            			<a class="permalink" href="#">Przejdz do wpisu »</a>
		            		</div>
		            	</a>
	            	</div>
            	</div>
            	<!-- blog-post-end -->

            	<!-- blog-post -->
            	<div class="col-md-4 col-sm-6 col-xs-12">
	            	<div class="item">
		            	<a href="">
		            		<img src="img/assets/b1.png" alt="b1">
		            		<div class="apla">
		            			<h3>Aenean nonummy hendrerit </h3>
		            			<span><time>02.02.2016</time> / Chwilówki</span>
		            			<p>
		            				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
		            			</p>
		            			<a class="permalink" href="#">Przejdz do wpisu »</a>
		            		</div>
		            	</a>
	            	</div>
            	</div>
            	<!-- blog-post-end -->

            	<!-- blog-post -->
            	<div class="col-md-4 col-sm-6 col-xs-12">
	            	<div class="item">
		            	<a href="">
		            		<img src="img/assets/b2.png" alt="b1">
		            		<div class="apla">
		            			<h3>Aenean nonummy hendrerit </h3>
		            			<span><time>02.02.2016</time> / Chwilówki</span>
		            			<p>
		            				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
		            			</p>
		            			<a class="permalink" href="#">Przejdz do wpisu »</a>
		            		</div>
		            	</a>
	            	</div>
            	</div>
            	<!-- blog-post-end -->

            	<!-- blog-post -->
            	<div class="col-md-4 col-sm-6 col-xs-12">
	            	<div class="item">
		            	<a href="">
		            		<img src="img/assets/b3.png" alt="b1">
		            		<div class="apla">
		            			<h3>Aenean nonummy hendrerit </h3>
		            			<span><time>02.02.2016</time> / Chwilówki</span>
		            			<p>
		            				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
		            			</p>
		            			<a class="permalink" href="#">Przejdz do wpisu »</a>
		            		</div>
		            	</a>
	            	</div>
            	</div>
            	<!-- blog-post-end -->

            	<!-- blog-post -->
            	<div class="col-md-4 col-sm-6 col-xs-12">
	            	<div class="item">
		            	<a href="">
		            		<img src="img/assets/b1.png" alt="b1">
		            		<div class="apla">
		            			<h3>Aenean nonummy hendrerit </h3>
		            			<span><time>02.02.2016</time> / Chwilówki</span>
		            			<p>
		            				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
		            			</p>
		            			<a class="permalink" href="#">Przejdz do wpisu »</a>
		            		</div>
		            	</a>
	            	</div>
            	</div>
            	<!-- blog-post-end -->

            	<!-- blog-post -->
            	<div class="col-md-4 col-sm-6 col-xs-12">
	            	<div class="item">
		            	<a href="">
		            		<img src="img/assets/b2.png" alt="b1">
		            		<div class="apla">
		            			<h3>Aenean nonummy hendrerit </h3>
		            			<span><time>02.02.2016</time> / Chwilówki</span>
		            			<p>
		            				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
		            			</p>
		            			<a class="permalink" href="#">Przejdz do wpisu »</a>
		            		</div>
		            	</a>
	            	</div>
            	</div>
            	<!-- blog-post-end -->

            	<!-- blog-post -->
            	<div class="col-md-4 col-sm-6 col-xs-12">
	            	<div class="item">
		            	<a href="">
		            		<img src="img/assets/b3.png" alt="b1">
		            		<div class="apla">
		            			<h3>Aenean nonummy hendrerit </h3>
		            			<span><time>02.02.2016</time> / Chwilówki</span>
		            			<p>
		            				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
		            			</p>
		            			<a class="permalink" href="#">Przejdz do wpisu »</a>
		            		</div>
		            	</a>
	            	</div>
            	</div>
            	<!-- blog-post-end -->
 
            </div>  
            
            </div> 
            <?php include("sidebar.php"); ?> 
        </div>
    </div>
</section>

<div class="bottom-baner">
    <div class="container">
    <div class="baner">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <h1>Brakuje Ci 200zł do końca miesiąca?</h1>
            <p>Złóż wniosek i otrzymaj gotówkę w 15min</p>
        </div>
    </div> 
    </div>
</div>

<?php include("footer.php"); ?>