<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Description Here">
    <meta name="author" content="Author Name">  

    <title>Porzyczkasa</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">  
    <!-- Custom styles for this template --> 
    <link href="css/main.css" rel="stylesheet">
    <!-- jQueryUi --> 
    <link href="css/jquery-ui.min.css" rel="stylesheet"> 
    
    <!-- Font OpenSams -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300italic,300' rel='stylesheet' type='text/css'>  
    <!-- Font Lato -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,100&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Font Awsome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    
    <!-- Favico --> 

  	<script src="js/modernizr.custom.js"></script>
  
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    
    <!--
    [if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]
    --> 
 
</head>
<body>
<div class="container">
    <div class="top">
        <a href="#" class="brand">
            <img src="img/logo.png" alt="logo">
        </a>
        <nav class="pull-right">
            <ul>
                <li><a href="/pozyczkasa">Strona Główna</a></li>
                <li><a href="page-o-nas.php">O nas</a></li>
                <li><a href="blog.php">Blog</a></li>
                <li><a href="template-kontakt.php">Kontakt</a></li>
            </ul>
        </nav>
    </div> 
</div>

<div class="container"> 
<!-- NAVBAR --> 
<nav class="navbar navbar-default" role="navigation"> 
<!-- Brand and toggle get grouped for better mobile display --> 
  <div class="navbar-header"> 
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> 
      <span class="sr-only">Toggle navigation</span> 
      <span class="icon-bar"></span> 
      <span class="icon-bar"></span> 
      <span class="icon-bar"></span> 
    </button>  
  </div> 
  <!-- Collect the nav links, forms, and other content for toggling --> 
  <div class="collapse navbar-collapse navbar-ex1-collapse"> 
    <ul class="nav navbar-nav"> 
      <li class="active"><a href="#">Chwilówki</a></li> 
      <li><a href="#">Kredyt gotówkowy </a></li>
      <li><a href="#"> Kredyt hipoteczny</a></li> 
      <li><a href="#">Konta osobiste</a></li>  
      <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Kredyty dla firm</a> 
        <ul class="dropdown-menu"> 
          <li><a href="#"><i></i>Darmowe Pożyczki</a></li> 
          <li><a href="#"><i></i>Pożyczki ratalne</a></li> 
          <li><a href="#"><i></i>kredyt na budowę domu</a></li> 
          <li><a href="#"><i></i>kredyt refinansowany</a></li> 
          <li><a href="#"><i></i>Kredyt konsolidacyjny</a></li>
          <li><a href="#"><i></i>Pozyczka hipoteczna</a></li>
          <li><a href="#"><i></i>kredyt walutowy</a></li>  
           
        </ul> 
      </li> 
    </ul>
  </div>
</nav>
<!-- NAVBAR -->
</div>
