<?php include("header.php"); ?>
<section id="pages"> 
    <div class="container"> 
        <div class="row">
            <div class="col-md-9 col-sm-8 col-xs-12">
            <h2 class="headline">Dane kontaktowe</h2>

            <div class="row kontakt-block">
                <div class="col-md-4 col-sm-4 col-xs-12 tel"> 
                        <img src="../img/ico-tel.png">  
                    <p>
                        +48 787 976 915
                    </p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 mail"> 
                        <img src="../img/ico-mail.png">  
                    <p>
                        biuro@pozyczkasa.pl
                    </p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 loc"> 
                        <img src="../img/ico-map.png">  
                    <p>
                        Marek Kozera Kozerus<br />
                        Jerzmanowice 72a, <br />32-048 Jerzmanowice
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs.12">
                    <form> 
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <i class="iname"></i>
                                <label for="exampleInputEmail1"> </label>
                                <input type="text" class="form-control" placeholder="Imię i nazwisko">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <i class="imail"></i>
                                <label for="exampleInputPassword1"> </label>
                                <input type="email" class="form-control" placeholder="Adres e-mail">
                            </div> 
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <i class="imess"></i>
                            <textarea class="form-control" rows="8" placeholder="Tresc wiadomości..."></textarea>
                        </div>      
                    </div> 
                        <button type="submit" class="btn btn-default btn-blue">Wyślij wiadomość</button>
                    </form>
                </div>
            </div>

            </div>

            <?php include("sidebar.php"); ?>

        </div>
    </div>
</section>
<?php include("footer.php"); ?>