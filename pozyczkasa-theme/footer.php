    <footer>
        <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
            <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
            </p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="newsletter">
                 <p>Zapisz się do newslettera..</p>
                    <div class="input-group">
                    <input type="text" class="form-control" aria-label="..." placeholder="Podaj e-mail">
                        <div class="input-group-btn"> 
                        <button type="button" class="btn btn-large btn-block btn-default blue-btn"><span class="glyphicon glyphicon-send"></span></button>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
            <ul>
                <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li> 
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            </ul>
            <small>© Copyright  by pozyczkasa.pl</small>
            </div>
        </div>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster --> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 
    <script src="js/jquery-ui.js"></script>
    <script src="js/main.js"></script>

    <script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */
    /*
    var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    */
    (function() {  // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        
        s.src = '//intblog.disqus.com/embed.js';
        
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
  
    </body>
</html> 