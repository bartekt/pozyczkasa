<?php include("header.php"); ?>
<div class="container"> 
    <div class="row">
        <div class="col-md-9 col-sm-8 col-xs-12">
            
        <!-- FILTROWANIE -->
        <div class="filtr">
            <div class="row">
                <div class="cost">
                    <p>Kwota pożyczki: <span> <input type="text" id="amount" readonly> ZŁ</span></p> 
                    <div class="slider-slide">
                        <div id="slider"></div>
                    </div> 
                </div>
                <div class="day"> 
                    <p>Okres spłaty:  <span> <input type="text" id="amount2" readonly> DNI</span></p> 
                    <div class="slider-slide">
                        <div id="slider2"></div>
                    </div>
                </div>
                <div class="for-free">
                    <p>Pokaż oferty:</p>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="">
                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                            <p>Za 0 zł (darmowe)</p>
                        </label>
                    </div> 
                </div>
            </div>
        </div> 
        <!-- FILTROWANIE -->

        <!-- LISTA -->

        <!-- ITEM -->
        <div class="col-md-12">
            <div class="row">
                <div class="product">
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="thumb-box">
                            <div class="thumb">
                                <img src="img/assets/a5.png"alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-43 col-sm-4 col-xs-12">
                    <p>
                    NetCredit udziela darmowej chwilówki do 2000 zł do 30 dni, przy pierwszej pożyczce. Dla stałych klientów Net Credit oferuje nawet 4000 zł! Firma w tej chwili udziela najwyższej darmowej pożyczki na rynku!
                    </p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 details">
                   	   <span class="headline">Pożyczasz</span>
                       <span class="cost">500zł</span>
                       <span class="headline">MOŻLIWY ZAKRES POŻYCZKI</span>
                       <ul>
                       <li> <span>Kwota:</span>   <p>Od <span>100 </span> zł</p>    <p>Do <span>1000 </span> zł</p> </li>
                       <li> <span>Okres:</span>    <p>Od <span>1 </span> Dnia</p>    <p>Do <span>30 </span> Dni</p> </li> 
                       </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="button-box">
                        <div class="buttons">
                          <button type="button" class="btn btn-large btn-block btn-default green">Złóż wniosek </button>
                          <button type="button" class="btn btn-large btn-block btn-default red">Dowiedz się więcej »</button>
                        </div>
                    </div> 
                    </div>
                </div>
            </div>
        </div> 
        <!-- ITEM -->

        <!-- ITEM 2-->
        <div class="col-md-12">
            <div class="row">
                <div class="product">
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="thumb-box">
                            <div class="thumb">
                                <img src="img/assets/a6.png"alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-43 col-sm-4 col-xs-12">
                    <p>
                    Filarum nie sprawdza swoich klientów w bazach BIK. Pierwszą pożyczkę na kwotę 1000 zł dostaniemy zupełnie za darmo na 30 dni. Sprawdź inne darmowe chwilówki.
                    </p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 details">
                   	   <span class="headline">Pożyczasz</span>
                       <span class="cost">500zł</span>
                       <span class="headline">MOŻLIWY ZAKRES POŻYCZKI</span>
                       <ul>
                       <li> <span>Kwota:</span>   <p>Od <span>100 </span> zł</p>    <p>Do <span>1000 </span> zł</p> </li>
                       <li> <span>Okres:</span>    <p>Od <span>1 </span> Dnia</p>    <p>Do <span>30 </span> Dni</p> </li> 
                       </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="button-box">
                        <div class="buttons">
                          <button type="button" class="btn btn-large btn-block btn-default green">Złóż wniosek </button>
                          <button type="button" class="btn btn-large btn-block btn-default red">Dowiedz się więcej »</button>
                        </div>
                    </div> 
                    </div>
                </div>
            </div>
        </div> 
        <!-- ITEM 2 -->

        <!-- ITEM 3-->
        <div class="col-md-12">
            <div class="row">
                <div class="product">
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="thumb-box">
                            <div class="thumb">
                                <img src="img/assets/a7.png"alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-43 col-sm-4 col-xs-12">
                    <p>
                    Latwy Kredyt udziela pożyczek w wysokości od 100 zł do 3000 zł na okres od 7 do 30 dni. Firma oferuje darmową pożyczkę do 1500 zł na okres 30 dni.
                    </p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 details">
                   	   <span class="headline">Pożyczasz</span>
                       <span class="cost">500zł</span>
                       <span class="headline">MOŻLIWY ZAKRES POŻYCZKI</span>
                       <ul>
                       <li> <span>Kwota:</span>   <p>Od <span>100 </span> zł</p>    <p>Do <span>1000 </span> zł</p> </li>
                       <li> <span>Okres:</span>    <p>Od <span>1 </span> Dnia</p>    <p>Do <span>30 </span> Dni</p> </li> 
                       </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="button-box">
                        <div class="buttons">
                          <button type="button" class="btn btn-large btn-block btn-default green">Złóż wniosek </button>
                          <button type="button" class="btn btn-large btn-block btn-default red">Dowiedz się więcej »</button>
                        </div>
                    </div> 
                    </div>
                </div>
            </div>
        </div> 
        <!-- ITEM 3 --> 
        <!-- LISTA --> 
    </div> 


    <?php include("sidebar.php"); ?> 


</div>
</div>

<div class="bottom-baner">
    <div class="container">
    <div class="baner">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <h1>Brakuje Ci 200zł do końca miesiąca?</h1>
            <p>Złóż wniosek i otrzymaj gotówkę w 15min</p>
        </div>
    </div> 
    </div>
</div>

<?php include("footer.php"); ?>