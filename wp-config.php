<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Skrypt wp-config.php używa tego pliku podczas instalacji.
 * Nie musisz dokonywać konfiguracji przy pomocy przeglądarki internetowej,
 * możesz też skopiować ten plik, nazwać kopię "wp-config.php"
 * i wpisać wartości ręcznie.
 *
 * Ten plik zawiera konfigurację:
 *
 * * ustawień MySQL-a,
 * * tajnych kluczy,
 * * prefiksu nazw tabel w bazie danych,
 * * ABSPATH.
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'pozyczkasa');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'pozyczkasa');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', 'pozyczkasa');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8mb4');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'I9sjDRO3VT`NZSvBa7Qbj-h ZSIs1aJt*T@tGE<(^6NU{^.]=44s0zAe9yn2b(cs');
define('SECURE_AUTH_KEY',  'J`6N;_Mp{XUE{NspGYMV0xiq5F%/2qk(j<z#C,nAp%z6p|V9GtKq)Uht:Aa=4c{5');
define('LOGGED_IN_KEY',    '|>lqhO>&mVlse&|0JH~)8#=v;$EmxC0zpu^^F?xJy?ig$+a<>q(kw6>z-hroHoI^');
define('NONCE_KEY',        '.BSA$[p&eW)SZk4z07KWYBK4X${oE eQpelr0(<p,8}z-v2jU,%euHb`wNpx`Cg ');
define('AUTH_SALT',        ',K*6=EGrFwdY>~L=QoM^;KI;;!r Vh_/y5|Z(VjDL%B!s/Da d`w6%=yUI+j*e|y');
define('SECURE_AUTH_SALT', 'kH839VOE=lOW;A wCl~HJhCoyb-wF}a~8xs@hbuc2d-j3^}fXn@p*8g}o3fAfgV+');
define('LOGGED_IN_SALT',   'PG@?)kxY]X8#CyHMgvJFZ!ta^mgrUqQyq_IaXlr5M0{6?17[BJX&DBW}mfA0I_7y');
define('NONCE_SALT',       '=+8pgkO%2v5^gU2}Ur.5-#Km0b_?a?Eri1L`Qh([K:||.z;Zo^]B=kq 4Gc1kF^|');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie
 * ostrzeżeń podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG podczas pracy nad nimi.
 *
 * Aby uzyskać informacje o innych stałych, które mogą zostać użyte
 * do debugowania, przejdź na stronę Kodeksu WordPressa.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
